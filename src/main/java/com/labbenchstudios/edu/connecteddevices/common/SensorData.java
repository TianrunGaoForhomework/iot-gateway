package com.labbenchstudios.edu.connecteddevices.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * this class is sense data class, which is for setting 
 * a series of values
 */
public class SensorData {
	String name = "Temperature";   // set name with temperature
	double curValue = 21.6;           // set curValue with 0
    double avgValue = 19.465;           // set avgValue with 0
	double minValue = 18.04;           // set minValue with 0
    double maxValue = 21.6;           // set maxValue with 0
    double totValue = 0;           // set totValue with 0
    int sampleCount = 1;           // set sampleCpunt with 0
	String timeStamp;              // create instance of String
    
    /*
     * SensorData constructor
     */
	public SensorData() {
    	// create Date instance 
		Date now = new Date();
    	// create simple date format instance
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	this.timeStamp = ft.format(now);
    }
    
    /*
     * the function to add value
     * @param newVal : newVal
     */
	public void addValue(double newVal) {
    	this.sampleCount = this.sampleCount + 1;
    	if(this.sampleCount == 1) {
    		this.minValue = newVal;
    	}
    	// get totValue
    	this.totValue = this.totValue + newVal;
    	// set curValue
    	this.curValue = newVal;
    	// if curValue < minValue
    	if(this.curValue < this.minValue) {
    		this.minValue = this.curValue;
    	}
    	// if CurVaule > minValue
    	if(this.curValue > this.maxValue) {
    		this.maxValue = this.curValue;
    	}
    	// if totVaule != 0 
    	if(this.totValue != 0 && this.sampleCount > 0) {
    		this.avgValue = this.totValue/this.sampleCount;
    	}
    }

	/*
	 * @return: String
	 */
	public String getName() {
		return name;
	}

	/*
	 * @return: Double
	 */
	public double getValue() {
		return curValue;
	}

	/*
	 * @return: Double
	 */
	public double getAvgValue() {
		return avgValue;
	}

	/*
	 * @return: Double
	 */
	public double getMinValue() {
		return minValue;
	}

	/*
	 * @return: Double
	 */
	public double getMaxValue() {
		return maxValue;
	}
	
	/*
	 * @return: Integer
	 */
	public int getSampleCount() {
		return sampleCount;
	}

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
	public String toString() {
    	// create an instance of StringBuilder
		StringBuilder info = new StringBuilder(" " + this.getName() + ":\n");
    	// append time
		info.append("\n" + "        Time: " + this.timeStamp + "\n");
    	// append value
		info.append("\n" + "        Current: " + this.getValue() + "\n");
    	// append avgvalue
		info.append("\n" + "        Average: " + this.getAvgValue() + "\n");
    	// append sampleCount
		info.append("\n" + "        Samples: " + this.getSampleCount() + "\n");
    	// append minValue
		info.append("\n" + "        Min: " + this.getMinValue() + "\n");
    	// append maxValue
		info.append("\n" + "        Max: " + this.getMaxValue() + "\n");
    	return info.toString();
    }
    
    /*
     * show information function
     */
	public void showInfo() {
    	System.out.println(this.toString());
    }	
}

