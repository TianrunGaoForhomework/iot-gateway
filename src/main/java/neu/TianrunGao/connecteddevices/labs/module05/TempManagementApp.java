/**
 * @author gaotianrun
 */
package neu.TianrunGao.connecteddevices.labs.module05;

/*
 * the script will simply initialize ‘tempSensorAdaptor’
 * and start the thread, then wait indefinitely
 */
public class TempManagementApp {

	public static void main(String[] args) {
		// create an instance of TempSensorAdaptor
		TempSensorAdaptor tempsensoradaptor = new TempSensorAdaptor();
		// set EnableAdaptorFlag with true
		tempsensoradaptor.setEnableAdaptorFlag(true);
		// thread start
		tempsensoradaptor.start();
	}

}
