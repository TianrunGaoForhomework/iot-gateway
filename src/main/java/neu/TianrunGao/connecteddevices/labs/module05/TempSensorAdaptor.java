package neu.TianrunGao.connecteddevices.labs.module05;
/*
 * import setting
 */
import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import com.labbenchstudios.edu.connecteddevices.common.DataUtil;
import com.labbenchstudios.edu.connecteddevices.common.SensorData;

/*
 * this is TempSensorAdaptor class, used for
 * read JSON file printing to the console and 
 * convert to instance, finally converting to 
 * JSON again
 */
public class TempSensorAdaptor extends Thread{
	private Thread t;                 // create instance thread t
	boolean enableAdaptor;            // create instance boolean 
	
	/*
	 * @param boolean : flag
	 */
	public void setEnableAdaptorFlag(boolean flag) {
		this.enableAdaptor = flag;
	}
	
	/*
	 * this fucntion is to read JSON data
	 * @ return: String 
	 */
	public String readString() throws IOException {
		// create an file instance from the file path
		File jsonfile = new File("/Users/gaotianrun/Desktop/result.json");
		// inputStream
		InputStream jsondata = new FileInputStream(jsonfile);
		// get jsondata size
		int size = jsondata.available();
		// create buffer getting from size
		byte[] buffer = new byte[size];
		// read data
		jsondata.read(buffer);
		// close file
		jsondata.close();
		String str = new String(buffer,"GB2312");
		// print to console
		System.out.println("reading JSON string...");
		System.out.println("JSON string:");
		System.out.println(str);
		return str;
	}
	/*
	 * overwrite run function
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		// if enableAdaptor is true
		if(enableAdaptor == true) {
			// create an instance of Datautil
			DataUtil datautil = new DataUtil();
			// create an instance of String
			String str = new String();
			try {
				// call readString
				str = this.readString();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// convert JSON to sensor data
			SensorData sensorData = datautil.jsonToSensorData(str);
			// print to console
			System.out.println("\n-------------------");
			System.out.println("New sensor readings:");
			sensorData.showInfo();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Thread#start()
	 */
	public void start() {
		System.out.println("TempSensorAdaptor starting...");
		if(t == null) {
			t = new Thread(this);
			t.start();
		}
	}

}

