package neu.TianrunGao.connecteddevices.labs.module06;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;
/*
 * This is MqttClientConnector class, which is for required 
 * callbacks for handing a message received, disconnect, and 
 * connection failure
 */
public class MqttClientConnector implements MqttCallback {
	// create static param logger
	private static final Logger _Logger = Logger.getLogger(MqttClientConnector.class.getName());
	// create an instance _MqttClientID
	private String _MqttClientID;
	// create an instance _brokerAddres
	private String _brokerAddress;
	// create an _Client
	private MqttClient _Client;
	// get ConfigConst protocol
	private String _protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	// get ConfigConst server
	private String _host = ConfigConst.DEFAULT_MQTT_SERVER;
	// get ConfigConst port
	private int _port = ConfigConst.DEFAULT_MQTT_PORT;
	
	public MqttClientConnector() {
		this(null, false);
	}
	/**
	 * Constructor for MqttClientConnector
	 * @param host: The name of the broker to connect.
	 * @param isSecure Currently unused.
	 */
	public MqttClientConnector(String host, boolean isSecure) {
		super();
		// NOTE: 'isSecure' ignored for now
		if (host != null && host.trim().length() > 0) {
			_host = host;
		}
		// NOTE: URL does not have a protocol handler for "tcp", construct the URL manually		
		_MqttClientID = MqttClient.generateClientId();
		// get logger
		_Logger.info("Using client ID for broker conn: " + _MqttClientID);
		// get brokerAddress
		_brokerAddress = _protocol + "://" + _host + ":" + _port;
		// get logger
		_Logger.info("Using URL for broker conn: " +  _brokerAddress);
	}
	
	/**
	* Connect to MqttClient:_Client
	*/
	public void connect() {
		if (_Client == null) {
			// create MemoryPersistence instance
			MemoryPersistence persistence = new MemoryPersistence();
			try {
				// create Client instance
				_Client = new MqttClient(_brokerAddress, _MqttClientID, persistence);
				// create MqttConnectOptions instance
				MqttConnectOptions cOptions = new MqttConnectOptions();
				// set cleanSession
				cOptions.setCleanSession(true);
				// set callback
				_Client.setCallback(this);
				// set connect
				_Client.connect(cOptions);
				// get logger
				_Logger.info("Connected to the broker: " + _brokerAddress);
			} catch (MqttException e) {
				_Logger.log(Level.SEVERE, "Failed!! to connect to the broker: " + _brokerAddress, e);
			}
		}
	}
	
	/**
	 * This method is to 
	 * disconnect to MqttClient:_Client
	 */
	public void disconnect() {
		try {
			// client disconnect
			_Client.disconnect();
			// get logger
			_Logger.info("Disconnected from the broker: " + _brokerAddress);
		} catch (Exception e) {
			_Logger.log(Level.SEVERE, "Failed!!! to disconnect from the broker: " +  _brokerAddress, e);
		}
	}
	
	/**
	 * Publishes the given payload to broker directly to topic 'topic'.
	 * @param topic: destination topic that the message direct to
	 * @param qosLevel: 0: at most once, 1: at least once, 2: exactly once
	 * @param payload
	 */
	public boolean publishMessage(String topic, int qosLevel, byte[] payload) {
		// set success as false
		boolean success = false;
		try {
			// get logger
			_Logger.info("Publishing message to topic: " + topic);
			//create a new MqttMessage, pass 'payload' to the constructor
			MqttMessage msg = new MqttMessage(payload);
			//set the QoS to qosLevel
			msg.setQos(qosLevel);
			//call 'publish' on the MQTT client, passing the 'topic' and MqttMessage
			msg.setRetained(true);
			// client to publish
			_Client.publish(topic, msg);
			success = true;
		} catch (Exception e) {
			// get logger
			_Logger.log(Level.SEVERE, "Failed!!! to publish MQTT message: " + e.getMessage());
		}
		return success;
	}
	
	/*
	 * This method is subscribeToAll
	 */
	public boolean subscribeToAll() {
		try {
			// client subscribe
			_Client.subscribe("$SYS/#");
			// get logger
			_Logger.log(Level.INFO, "Subscribe to all successfully.");
			return true;
		} catch (MqttException e) {
			// get logger
			_Logger.log(Level.WARNING, "Failed!! to subscribe to all topics.", e);
		}
		return false;
	}
	/*
	 * This method is subscribeToTopic
	 * @param topic : String
	 */
	public boolean subscribeToTopic(String topic) {
		try {
			// client subscribe
			_Client.subscribe(topic);
			// get logger
			_Logger.log(Level.INFO, "Subscribe to Topic successfully.");
			return true;
		} catch (MqttException e) {
			// get logger
			_Logger.log(Level.WARNING, "Failed!! to subscribe to Topic topics.", e);
		}
		return false;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#connectionLost(java.lang.Throwable)
	 */
	public void connectionLost(Throwable t) {
		_Logger.log(Level.WARNING, "Connection lost.....", t);
	}

	/*
	 * This method is deliveryssComplete
	 * @param IMqttDeliveryToken : token
	 */
	public void deliveryssComplete(IMqttDeliveryToken token) {
		try {
			// get logger
			_Logger.info("Delivery complete: " + token.getMessageId() + " - " + token.getResponse() + " - "
					+ token.getMessage());
		} catch (Exception e) {
			// get logger
			_Logger.log(Level.SEVERE, "Failed!! to retrieve message from token.", e);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#messageArrived(java.lang.String, org.eclipse.paho.client.mqttv3.MqttMessage)
	 */
	public void messageArrived(String data, MqttMessage msg) throws Exception {
		_Logger.info("Message arrived: " + data + ", " + msg.getId() + ", " + msg.toString());
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#deliveryComplete(org.eclipse.paho.client.mqttv3.IMqttDeliveryToken)
	 */
	public void deliveryComplete(IMqttDeliveryToken token) {
		
	}

}
