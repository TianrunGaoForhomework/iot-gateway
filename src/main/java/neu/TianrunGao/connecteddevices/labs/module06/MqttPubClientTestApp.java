package neu.TianrunGao.connecteddevices.labs.module06;

import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.gson.Gson;
import com.labbenchstudios.edu.connecteddevices.common.SensorData;
/*
 * This is MqttPubClientTestApp class, which is for
 * publishing a simple message to the test topic and 
 * exit
 */
public class MqttPubClientTestApp {
	// set static params of logger
	private static final Logger _Logger = Logger.getLogger(MqttPubClientTestApp.class.getName());
	// create static  MqttPubClientTestApp instance
	private static MqttPubClientTestApp _App;
	// create MqttClientConnector instance
	private MqttClientConnector _Client;
	/*
	 * constructor of MqttPubClientTestApp
	 */
	public MqttPubClientTestApp() {
		
	}
	/*
	 * @param args : arguments
	 */
	public static void main(String[] args) {
		// create an instance of MqttPubClientTestApp
		_App = new MqttPubClientTestApp();
		try {
			_App.start();
		} catch (Exception e) {
			_Logger.log(Level.WARNING, "Failed!! to start _App.", e);
		}
	}
	/**
	 * This method is to connect to the MQTT client 
	 * and publish a test message to the given topic
	 */
	public void start() {
		//Create a new MqttClient:_Client and connection
		_Client = new MqttClientConnector();
		// create an instance of SensorData
		SensorData _SensorData = new SensorData();
		// connect
		_Client.connect();
		//Set topic 
		String topic = "AssignmentForMQTT";
		// Set the payload for publishing...
		Gson gson = new Gson();
		// String to JSON format
		String json = gson.toJson(_SensorData);
		// publish payload with the topic in Qoslevel 2
		_Client.publishMessage(topic, 1, json.getBytes());
		// disconnect
		_Client.disconnect();
	}
}
