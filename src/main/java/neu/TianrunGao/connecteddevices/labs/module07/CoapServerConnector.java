package neu.TianrunGao.connecteddevices.labs.module07;

import java.util.logging.Logger;


import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;

/*
 * this is CoapServerConnector class, which will instance
 * the requisite handlers and listen for connections
 */
public class CoapServerConnector {
	 //static
	 private static final Logger _Logger = Logger.getLogger(CoapServerConnector.class.getName());
	 // private var's
	 private CoapServer _coapServer;
	 // constructors
	 public CoapServerConnector() {
	  super();
	 }
	 
	 /* public addResource method
	  * @param resource : CoapResource
	  */
	 public void addResource(CoapResource resource) {
	  // if not null
		 if(resource != null) {
	   // add
			 _coapServer.add(resource);
	  }
	  }
	 
	 // start method 
	 public void start() {
	  // if null
		 if(_coapServer == null) {
	   // logger
		  _Logger.info("Creating CoAP server instance and 'temp' handler...");
	   _coapServer = new CoapServer();
	   // implement TempResourceHandler
	   TempResourceHandler tempHandler = new TempResourceHandler();
	   // add
	   _coapServer.add(tempHandler);
	   
	  }
	  // make logger
		 _Logger.info("Starting CoAP server...");
	  _coapServer.start();
	 }
	 
	 // stop server
	 public void stop() {
	  // nake logger
		 _Logger.info("Stopping CoAP server...");
	  _coapServer.stop();
	 }
	}