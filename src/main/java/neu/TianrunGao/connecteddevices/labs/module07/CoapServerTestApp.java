package neu.TianrunGao.connecteddevices.labs.module07;
/*
 * this is the CoapServerTestApp class, which is for
 * running a number of tests, then exit
 */
public class CoapServerTestApp {
	// create static instance 
	private static CoapServerTestApp _App;
	 public static void main(String[] args) {
	  // create instance of CoapServerTestApp
		 _App = new CoapServerTestApp();
	  try {
	   // start
		  _App.start();
	  }catch(Exception e) {
	   e.printStackTrace();
	  }
	 }
	 // private var's
	 private CoapServerConnector _coapServer;
	 // constructor
	 public CoapServerTestApp() {
	  super();
	 }
	 //public methods
	 public void start() {
	  // create instance of _coapServer
		 _coapServer = new CoapServerConnector();
	  // start
		 _coapServer.start();
	 }
	}
