package neu.TianrunGao.connecteddevices.labs.module07;

import java.util.logging.Logger;


import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import com.labbenchstudios.edu.connecteddevices.common.SensorData;
import com.google.gson.Gson;
import com.labbenchstudios.edu.connecteddevices.common.DataUtil;
/*
 * this is TempResourceHandler class, which for handling in
 * GET, POST, PUT, DELETE and Converting
 */
public class TempResourceHandler extends CoapResource {
	 
	 // static 
	 private static final Logger _Logger = Logger.getLogger(TempResourceHandler.class.getName());
	 // create SensorData instance
	 private SensorData sd = new SensorData();
	 // create Gson instance
	 private Gson gson = new Gson();

	 /**
	  * Default constructor, set the name to "temp"
	  */
	 public TempResourceHandler() {
	  super("temp");
	 }
	 
	 /**
	  * Constructor, set the name
	  * @param name: name of the handler 
	  */
	 public TempResourceHandler(String name) 
	 {
	  super(name);
	 }
	 
	 /**
	  * Constructor, set the name
	  * @param name: name of the handler 
	  * @param visible: flag showing if the handler is visible
	  */
	 public TempResourceHandler(String name, boolean visible) 
	 {
	  super(name, visible);
	 }
	 
	 /**
	  * GET method, this function handles the GET method
	  * @param ce: the CoAP Exchange 
	  */
	 public void handleGET(CoapExchange ce) {
	  ce.respond(ResponseCode.CREATED, "GET worked!");
	  _Logger.info("Received GET request from client...");
	 }
	 
	 /**
	  * POST method, this function handles the POST method
	  * It outputs the text of the response to stdout.
	  * @param ce: the CoAP Exchange 
	  */
	 public void handlePOST(CoapExchange ce) {
		 //print 
	  System.out.println("The SensorData in JSON format POST by the client is:");
	  // logger
	  _Logger.info("Receive Json format");
	  System.out.println(ce.getRequestText());
	  // create DataUtil instance
	  DataUtil du = new DataUtil();
	  // Convert Json format to sensorData
	  sd = du.jsonToSensorData(ce.getRequestText());
	  // logger
	  _Logger.info("Cransfer JSON into SensorData instance: ");
	  // print to string
	  sd.showInfo();
	  // Convert sensorData to Json
	  String payload = gson.toJson(sd);
	  // logger
	  _Logger.info("Convert to Json");
	  // logger
	  _Logger.info(payload);
	  // response content
	  ce.respond(ResponseCode.CONTENT, payload);
	  // logger
	  _Logger.info("Received POST request from client...");
	 }
	 
	 /**
	  * PUT method, this function handles the PUT method
	  * @param ce: the CoAP Exchange 
	  */
	 public void handlePUT(CoapExchange ce) {
	  ce.respond(ResponseCode.CONTENT, "PUT worked!");
	  _Logger.info("Received PUT request from client...");
	 }
	 
	 /**
	  * DELETE method, this function handles the DELETE method
	  * @param ce: the CoAP Exchange 
	  */
	 public void handleDELETE(CoapExchange ce) {
	  ce.respond(ResponseCode.DELETED, "DELETE worked!");
	  _Logger.info("Received DELETE request from client...");
	 }
	}
