package neu.TianrunGao.connecteddevices.labs.module08;

import java.util.logging.Level;

import java.util.logging.Logger;

import neu.TianrunGao.connecteddevices.labs.module08.MqttClientConnector;

/*
 * this is TempActuatorSubscriberApp class, whcih is for 
 * Connecting securely to Ubidots device using MQTT
 */
public class TempActuatorSubscriberApp {
	// set static userName
	private String _userName = "A1E-M81iAlSNOgak24OtTJklfkrl8IcMyg";
	// set static authToken
	private String _authToken = null;
	// set static pemfileName
	private String _pemFileName = "/Users/gaotianrun/Desktop/ubidots.pem";
	// set static host 
	private String _host = "things.ubidots.com";
	// create logger
	private static final Logger _Logger = Logger.getLogger(TempActuatorSubscriberApp.class.getName());
	// create static instance of TempActuatorSubscriberApp
	private static TempActuatorSubscriberApp _App;
	// create static instance MqttClientConnector
	private MqttClientConnector _Client;

	/*
	 * main method to run TempActuatorSubscriberApp
	 * @param args : String[]
	 */
	public static void main(String[] args) {
		// create instance
		_App = new TempActuatorSubscriberApp();
		try {
			// run start
			_App.start();
		} catch (Exception e) {
		 	// get logger
			_Logger.log(Level.WARNING, "Failed!! to start _App.", e);
		}
	}

	/*
	 * constructor
	 */
	public TempActuatorSubscriberApp() {
		super();
	}
	
	/*
	 * start method 
	 */
	public void start() {
		// get host, username, pemfilename, authtoken value
		_Client = new MqttClientConnector(_host, _userName, _pemFileName, _authToken);
		// connect
		_Client.connect();
		//Subscribe with the topic
		String topic = "/v1.6/devices/gtrdevice/tempactuator";
		_Client.subscribeToTopic(topic); 
		//Subscribe with the topic2
		String topic2 = "/v1.6/devices/gtrdevice/tempsensor";
		_Client.subscribeToTopic(topic2);
	}
}