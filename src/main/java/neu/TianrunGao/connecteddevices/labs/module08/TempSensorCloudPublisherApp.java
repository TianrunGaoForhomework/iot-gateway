package neu.TianrunGao.connecteddevices.labs.module08;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.ubidots.*;

/*
 * this class is TempSensorCloudPublisherApp, which is for
 * Connecting securely to Ubidots device using HTTPS
 */
public class TempSensorCloudPublisherApp 
{
 // create logger
 private static final Logger _Logger = Logger.getLogger(TempSensorPublisherApp.class.getName());
 // create static instance of TempSensorCloudPublisherApp
 private static TempSensorCloudPublisherApp _App;
 
 /**
  * Default constructor
  */
 public TempSensorCloudPublisherApp() 
 {
  super();
 }

 /**
  * Start to run the  application, 
  * connecting to the MQTT client and
  * publish message to the topic
  */
 public void start() 
 {
  // set api id
	 ApiClient api = new ApiClient("A1E-de9c8e631c15c2206f963760aafbfd90f9c2");
  // create instance of DataSource
	 DataSource dataSource = api.getDataSource("5c997dffc03f97266558c1b6");
  // create set of Variable
	 Variable[] variable = dataSource.getVariables();
        try
        {
         while(true)
   {
          System.out.println("successful");
    // set random 
          Random r = new Random();
    // get range of number
          float temp  = r.nextFloat()*30;
    variable[1].saveValue(10);
    // to run at interval
    Thread.sleep(3*1000);
   }
        }
        catch(Exception ex)
        {  
            ex.printStackTrace();  
        }    
 }
 
 /**
  * Main function
  * @param args: command line arguments
  */
 public static void main(String[] args) 
 {
  // create instance of TempSensorCloudPublisherApp
	 _App = new TempSensorCloudPublisherApp();
  try 
  {
   // run to start
	  _App.start();
  } 
  catch (Exception e) 
  {
   // get logger
	  _Logger.log(Level.WARNING, "Failed!! to start _App.", e);
  }
 }
}