package neu.TianrunGao.connecteddevices.labs.module08;

import java.util.Random;

import java.util.logging.Level;
import java.util.logging.Logger;
import neu.TianrunGao.connecteddevices.labs.module08.MqttClientConnector;

/*
 * this is TempSensorPublisherApp class, which is for 
 * Connecting securely to  Ubidots device using MQTT (ssl)
 */
public class TempSensorPublisherApp {
	// set static username
	private String _userName = "A1E-M81iAlSNOgak24OtTJklfkrl8IcMyg";
	// set static authtoken
	private String _authToken = null;
	// set static pemfilename
	private String _pemFileName = "/Users/gaotianrun/Desktop/ubidots.pem";
	// set static host
	private String _host = "things.ubidots.com";
	// create logger
	private static final Logger _Logger = Logger.getLogger(TempSensorPublisherApp.class.getName());
	//create static instance TempSensorPublisherApp
	private static TempSensorPublisherApp _App;
	// create static instance MqttClientConnector
	private MqttClientConnector _Client;
	
	/*
	 * constructor of TempSensorPublisherApp
	 */
	public TempSensorPublisherApp() {
	}
	
	/*
	 * main method to run TempSensorPublisherApp
	 * @param args : String[]
	 */
	public static void main(String[] args) {
		// create instance of TempSensorPublisherApp
		_App = new TempSensorPublisherApp();
		try {
			// run start
			_App.start();
		} catch (Exception e) {
			// get logger
			_Logger.log(Level.WARNING, "Failed!! to start _App.", e);
		}
	}
	
	/*
	 * start method
	 */
	public void start() {
		// // get host, username, pemfilename, authtoken value
		_Client = new MqttClientConnector(_host, _userName, _pemFileName, _authToken);
		// connect
		_Client.connect();
		// set the topic
		String topic = "/v1.6/devices/gtrdevice/tempsensor";
		try {
	         while (true) {
	          // get random value
	        	 Random r = new Random();
	          // get value range
	        	 float val  = r.nextFloat()*30;
	          // int to string 
	        	 String str = String.valueOf(val);
	          // publish
	        	 _Client.publishMessage(topic, 0, str.getBytes());      //publish payload with the topic in Qoslevel 2
	           // get interval
	        	 Thread.sleep(60*1000);
	         }
	     } catch (InterruptedException e) {
	         e.printStackTrace();
	     }
	   // disconnect
		_Client.disconnect();
	}

}
