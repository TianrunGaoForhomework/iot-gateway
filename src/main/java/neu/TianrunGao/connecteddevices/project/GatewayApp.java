package neu.TianrunGao.connecteddevices.project;
import java.util.logging.Logger;

/*
 * This is Gatewayapp class, which is for running the 
 * whole java program. first we activate coapServerConnection
 * file
 */
public class GatewayApp{
	
	//private var's for logger
	private static final Logger _Logger =
	 Logger.getLogger(GatewayApp.class.getName());
	// create instance of Gatewayapp
	private static GatewayApp _APP;
	// create instance of CoapServerConnection
	private CoapServerConnection _Server;
	
	/**
	 * Constructor.
	 * Default
	 *
	 */
	public GatewayApp() {
		
	}
	
	/**
	 * Start COAP server method
	 *
	 */
	public void start() {
		// create instance of CoapServerConnection	
		_Server = new CoapServerConnection();
		// to run	
		_Server.start();

	}
	/**
	 * main method
	 *
	 */
	public static void main(String[] args) {
		// create instance of GatewayApp
		 _APP = new GatewayApp();
		 try {
			 // create logger
			 _Logger.info("ServerApp.start!!");
			 //coap server start
			 _APP.start();
		 } catch (Exception e) {
			 // create logger
			 _Logger.info("ServerApp.start Failed!!");
		 }
		
	}
	
}
