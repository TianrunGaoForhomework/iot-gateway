package neu.TianrunGao.connecteddevices.project;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;


/*
 * This is MqttClientConnector class, which is for required 
 * callbacks for handing a message received, disconnect, and 
 * connection failure
 */
public class MqttClientConnector implements MqttCallback {
	// create static param logger
	private static final Logger _Logger = Logger.getLogger(MqttClientConnector.class.getName());
	// create an instance _MqttClientID
	private String _MqttClientID;
	// create an instance _brokerAddres
	private String _brokerAddress;
	// create an _Client
	private MqttClient _Client;
	// get ConfigConst protocol
	private String _protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	// get ConfigConst server
	private String _host = "things.ubidots.com";
	// get ConfigConst port
	private int _port;
	// get user name
	private String _userName;
	// get user password
	private String _password;
	// set pem file
	private String _pemFileName;
	// set boolean
	private Boolean _isSecureConn;
	
	private MqttConnGateToDev _ClientGTD;
	
	/*
	 * Constructor of MqttClientConnector
	 */
	public MqttClientConnector() {
		this(null, false);
	}
	/**
	 * Constructor for MqttClientConnector
	 * @param host: The name of the broker to connect.
	 * @param isSecure Currently unused.
	 */
	public MqttClientConnector(String host, boolean isSecure) {
		super();
		// NOTE: 'isSecure' ignored for now
		if (host != null && host.trim().length() > 0) {
			_host = host;
		}
		// NOTE: URL does not have a protocol handler for "tcp", construct the URL manually		
		_MqttClientID = MqttClient.generateClientId();
		// get logger
		_Logger.info("Using client ID for broker conn: " + _MqttClientID);
		// get brokerAddress
		_brokerAddress = _protocol + "://" + _host + ":" + _port;
		// get logger
		_Logger.info("Using URL for broker conn: " +  _brokerAddress);
	}
	
	/**
	 * Constructor.
	 * @param host : String
	 * @param userName : String
	 * @param pemFileName : String
	 * @param password : String
	 */
	public MqttClientConnector(String host, String userName, String pemFileName, String password) {
		super();
		// if host not null
		if (host != null && host.trim().length() > 0) {
			// set host
			_host = host;
		}
		// if user name not null
		if (userName != null && userName.trim().length() > 0) {
			// set userName
			_userName = userName;
		}
		// if pemFileName not null
		if (pemFileName != null) {
			// create file
			File file = new File(pemFileName);
			// if file not null
			if (file.exists()) {
				// set protocol value
				_protocol = "ssl";
				// set port value
				_port = 8883;
				// set pemFileName value
				_pemFileName = pemFileName;
				// set isSecureConn value
				_isSecureConn = true;
				// make log
				_Logger.info("PEM file valid. Using secure connection: " + _pemFileName);
			} else {
				_Logger.warning("PEM file invalid. Using insecure connection: " + pemFileName);
			}
		}
		
		// NOTE: URL does not have a protocol handler for "tcp", construct the URL manually
		_MqttClientID = MqttClient.generateClientId();
		// broker address
		_brokerAddress = _protocol + "://" + _host + ":" + _port;
		_Logger.info("Using URL for broker conn: " + _brokerAddress);
	}
	/**
	* Connect to MqttClient:_Client
	*/
	public void connect() {
		if (_Client == null) {
			// create MemoryPersistence instance
			MemoryPersistence persistence = new MemoryPersistence();
			try {
				// create Client instance
				_Client = new MqttClient(_brokerAddress, _MqttClientID, persistence);
				// create MqttConnectOptions instance
				MqttConnectOptions cOptions = new MqttConnectOptions();
				// set cleanSession
				cOptions.setCleanSession(true);
				// set callback
				if (_userName != null) {
					// set user name
					cOptions.setUserName(_userName);
				}
				if (_password != null) {
					// set password
					cOptions.setPassword(_password.toCharArray());
				}
				if (_isSecureConn) {
					// init secure connection
					initSecureConnection(cOptions);
				}
				_Client.setCallback(this);
				// set connect
				_Client.connect(cOptions);
				// get logger
				_Logger.info("Connected to the broker: " + _brokerAddress);
			} catch (MqttException e) {
				_Logger.log(Level.SEVERE, "Failed!! to connect to the broker: " + _brokerAddress, e);
			}
		}
	}
	
	/**
	 * This method is to 
	 * disconnect to MqttClient:_Client
	 */
	public void disconnect() {
		try {
			// client disconnect
			_Client.disconnect();
			// get logger
			_Logger.info("Disconnected from the broker: " + _brokerAddress);
		} catch (Exception e) {
			_Logger.log(Level.SEVERE, "Failed!!! to disconnect from the broker: " +  _brokerAddress, e);
		}
	}
	
	/**
	 * Publishes the given payload to broker directly to topic 'topic'.
	 * @param topic: destination topic that the message direct to
	 * @param qosLevel: 0: at most once, 1: at least once, 2: exactly once
	 * @param payload
	 */
	public boolean publishMessage(String topic, int qosLevel, byte[] payload) {
		// set success as false
		boolean success = false;
		try {
			// get logger
			_Logger.info("Publishing message to topic: " + topic);
			//create a new MqttMessage, pass 'payload' to the constructor
			MqttMessage msg = new MqttMessage(payload);
			//set the QoS to qosLevel
			msg.setQos(qosLevel);
			//call 'publish' on the MQTT client, passing the 'topic' and MqttMessage
			msg.setRetained(true);
			// client to publish
			_Client.publish(topic, msg);
			success = true;
		} catch (Exception e) {
			// get logger
			_Logger.log(Level.SEVERE, "Failed!!! to publish MQTT message: " + e.getMessage());
		}
		return success;
	}
	
	/*
	 * This method is subscribeToAll
	 */
	public boolean subscribeToAll() {
		try {
			// client subscribe
			_Client.subscribe("$SYS/#");
			// get logger
			_Logger.log(Level.INFO, "Subscribe to all successfully.");
			return true;
		} catch (MqttException e) {
			// get logger
			_Logger.log(Level.WARNING, "Failed!! to subscribe to all topics.", e);
		}
		return false;
	}
	/*
	 * This method is subscribeToTopic
	 * @param topic : String
	 */
	public boolean subscribeToTopic(String topic) {
		try {
			// client subscribe
			_Client.subscribe(topic);
			// get logger
			_Logger.log(Level.INFO, "Subscribe to Topic successfully.");
			return true;
		} catch (MqttException e) {
			// get logger
			_Logger.log(Level.WARNING, "Failed!! to subscribe to Topic topics.", e);
		}
		return false;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#connectionLost(java.lang.Throwable)
	 */
	public void connectionLost(Throwable t) {
		_Logger.log(Level.WARNING, "Connection lost.....", t);
	}

	/*
	 * This method is deliveryssComplete
	 * @param IMqttDeliveryToken : token
	 */
	public void deliveryssComplete(IMqttDeliveryToken token) {
		try {
			// get logger
			_Logger.info("Delivery complete: " + token.getMessageId() + " - " + token.getResponse() + " - "
					+ token.getMessage());
		} catch (Exception e) {
			// get logger
			_Logger.log(Level.SEVERE, "Failed!! to retrieve message from token.", e);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#messageArrived(java.lang.String, org.eclipse.paho.client.mqttv3.MqttMessage)
	 */
	public void messageArrived(String data, MqttMessage msg) throws Exception {
		_Logger.info("Message arrived: " + data + ", " + msg.getId());
		_Logger.info("Message plyload: "+ msg.toString());
		System.out.println("=================================");
		
		/**
		 * Starting the MQTT publish to the new topic ActuatorData
		 * The iot-device will subscribe this topic and get the payload
		 * Using qos level 1.
		 * disconnect after published.
		 * 
		 */
		if (msg.getId() == 1) {
			System.out.println("======================Start Sending Data Demo======================");
			System.out.print("Sending Actuator Data to the iot-Device.....\n");
			// set sting payload
			String payload = msg.toString();
			// system print
			System.out.println(payload);
			// create instance of MqttConnGateToDev
			_ClientGTD = new MqttConnGateToDev();
			// client connect
			_ClientGTD.connect();
			//Set topic 
			String topicGTD = "ActuatorData";
			// Set the payload for publishing...
			//publish payload with the topic in Qoslevel 2
			_ClientGTD.publishMessage(topicGTD, 1, payload.getBytes());
			_ClientGTD.disconnect();
		    // system print
			System.out.println("-----------------Sending Data Complete-------------------\n");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#deliveryComplete(org.eclipse.paho.client.mqttv3.IMqttDeliveryToken)
	 */
	public void deliveryComplete(IMqttDeliveryToken token) {
		
	}
	
	/*
	 * this method is for read certificate 
	 */
	private KeyStore readCertificate()
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		// create instance of KeyStore
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		// input file
		FileInputStream fis = new FileInputStream(_pemFileName);
		// 
		BufferedInputStream bis = new BufferedInputStream(fis);
		// create instance of Certificate factory
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		// load
		ks.load(null);
		while (bis.available() > 0) {
			// create instance of Certificate 
			Certificate cert = cf.generateCertificate(bis);
			//Set the device name here
			ks.setCertificateEntry("homeiotgateway" + bis.available(), cert);

		}
		return ks;
	}
	
	/*
	 * this method is for init secure connection 
	 * @param connOpts : MqttConnectOptions 
	 */
	private void initSecureConnection(MqttConnectOptions connOpts) {
		try {
			// get logger
			_Logger.info("Configuring TLS...");
			// create instance of SSL Context
			SSLContext sslContext = SSLContext.getInstance("SSL");
			// create instance of KeyStore
			KeyStore keyStore = readCertificate();
			// create instance of TrustManager Factory
			TrustManagerFactory trustManagerFactory = TrustManagerFactory
					.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			// init trust manager factory
			trustManagerFactory.init(keyStore);
			// init ssl context
			sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
			// set socket factory
			connOpts.setSocketFactory(sslContext.getSocketFactory());
		} catch (Exception e) {
			_Logger.log(Level.SEVERE, "Failed to initialize secure MQTT connection.", e);
		}
	}

}
