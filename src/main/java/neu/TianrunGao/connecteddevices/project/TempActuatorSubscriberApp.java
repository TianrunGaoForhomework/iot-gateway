package neu.TianrunGao.connecteddevices.project;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tianrun Gao
 *
 */

/*
 * this is TempActuatorSubscriberApp class, whcih is for 
 * Connecting securely to Ubidots device using MQTT
 */
public class TempActuatorSubscriberApp {
	// set static userName
	private String _userName = "A1E-M81iAlSNOgak24OtTJklfkrl8IcMyg";
	// set static authToken
	private String _authToken = null;
	// set static pemfileName
	private String _pemFileName = "/Users/gaotianrun/Desktop/ubidots.pem";
	// set static host 
	private String _host = "things.ubidots.com";
	// private var's static
	private static final Logger _Logger = Logger.getLogger(TempActuatorSubscriberApp.class.getName());
	// create static instance of TempActuatorSubscriberApp
	private static TempActuatorSubscriberApp _App;
	// create static instance of MqttClientConnector
	private MqttClientConnector _Client;
	
	/*
	 * main method to run TempActuatorSubscriberApp
	 * @param args : String[]
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// create instance
		_App = new TempActuatorSubscriberApp();
		try {
			// run start
			_App.start();
		} catch (Exception e) {
			// create logger
		 	_Logger.log(Level.WARNING, "Failed!! to start _App.", e);
		}
	}
	
	// constructors
	/**
	 * Default.
	 */

	public TempActuatorSubscriberApp() {
		super();
	}
	
	// public methods
	/**
	 * Connect to the MQTT client, then: 1) If this is the subscribe app, subscribe
	 * to the given topic 2) If this is the publish app, publish a test message to
	 * the given topic
	 * 
	 * We will use subscribe app here.
	 * And we catch 5 vars here. 
	 * They are tempactuator; tempsensor; tempsensormin; tempsensormax; tempsensoravg;
	 */
	public void start() {
		// get host, username, pemfilename, authtoken value
		_Client = new MqttClientConnector(_host, _userName, _pemFileName, _authToken);
		// connect
		_Client.connect();
		//Subscribe with the topic
		String topic = "/v1.6/devices/gtrdevice/tempactuator";
		_Client.subscribeToTopic(topic); 
		//Subscribe with the topic2
		String topic2 = "/v1.6/devices/gtrdevice/tempsensor";
		_Client.subscribeToTopic(topic2);
		//Subscribe with the topicMin
		String topicMin = "/v1.6/devices/gtrdevice/tempsensormin";
		_Client.subscribeToTopic(topicMin); 
		//Subscribe with the topicMax
		String topicMax = "/v1.6/devices/homeiotgateway/tempsensormax";
		_Client.subscribeToTopic(topicMax);
		//Subscribe with the topicAvg
		String topicAvg = "/v1.6/devices/homeiotgateway/tempsensoravg";
		_Client.subscribeToTopic(topicAvg); 
		//Subscribe with the topicHum
		String topicHum = "/v1.6/devices/homeiotgateway/humsensor";
		_Client.subscribeToTopic(topicHum); 
		//Subscribe with the topicMinForHum
		String topicMinForHum = "/v1.6/devices/homeiotgateway/humsensormin";
		_Client.subscribeToTopic(topicMinForHum); 
		//Subscribe with the topicMaxForHum
		String topicMaxForHum = "/v1.6/devices/homeiotgateway/humsensormax";
		_Client.subscribeToTopic(topicMaxForHum); 
	}
}
