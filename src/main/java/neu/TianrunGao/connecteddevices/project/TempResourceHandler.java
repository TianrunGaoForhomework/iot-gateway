	package neu.TianrunGao.connecteddevices.project;

import java.util.logging.Logger;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

/*
 * this is TempResourceHandler class, which for handling in
 * GET, POST, PUT, DELETE and Converting
 */
public class TempResourceHandler extends CoapResource{
	// create logger instance
	private static final Logger _Logger = Logger.getLogger(TempResourceHandler.class.getName());
	// create userName
	private String _userName = "A1E-M81iAlSNOgak24OtTJklfkrl8IcMyg";
	// create authToken
	private String _authToken = null;
	// create pemFileName
	private String _pemFileName = "/Users/gaotianrun/Desktop/ubidots.pem";
	// create host
	private String _host = "things.ubidots.com";
	// create instance of MqttClientConnector
	private MqttClientConnector _Client;
	
	/**
	  * Default constructor, set the name to "temp"
	  */
	 public TempResourceHandler() {
	  super("temp");
	 }
	 
	 /**
	  * Constructor, set the name
	  * @param name: name of the handler 
	  */
	 public TempResourceHandler(String name) 
	 {
	  super(name);
	 }
	 
	 /**
	  * Constructor, set the name
	  * @param name: name of the handler 
	  * @param visible: flag showing if the handler is visible
	  */
	 public TempResourceHandler(String name, boolean visible) 
	 {
	  super(name, visible);
	 }

		@Override
		/**
		 * Post is to create a resource
		 * Add MQTT part:
		 * After handlePOST, we can get the temp value from senseHat by COAP protocol
		 * And the value would get by ce.getRequestText() and send it to mqtt payload.
		 * Then, we use mqtt protocol--publish the value to the broker, cloud. 
		 * Final, the ubidots will get the sensor value from senseHat and decide to upload the event or not.
		 */
		public void handlePOST(CoapExchange ce) {
			// create String response message
			String responseMsg = "Here's the reponse to temp request::" + super.getName();
            // print request
			System.out.println(ce.getRequestText());
			// send response code
			ce.respond(ResponseCode.VALID, responseMsg);
			// create instance of MqttClientConnector
			_Client = new MqttClientConnector(_host, _userName, _pemFileName, _authToken);
			//_ClientPC.connectPC();	
			_Client.connect();
			// get string payload
			String payload = ce.getRequestText();
			// get topic for temperature
			String topicS = "/v1.6/devices/gtrdevice/tempsensor";
			// get topic for humidity
			String topicH = "/v1.6/devices/gtrdevice/humsensor";
			// if payload contains T
			if(payload.contains("T")) {
				// publish message
				_Client.publishMessage(topicS, 0, payload.substring(1).getBytes());
			}
			// if payload contains H
			if(payload.contains("H")) {
				// publish message
				_Client.publishMessage(topicH, 0, payload.substring(1).getBytes());
			}
			// client disconnect
			_Client.disconnect();
			// create logger
			_Logger.info("Handling POST:" + responseMsg);
			// create logger
			_Logger.info(ce.getRequestCode().toString() + ": " + ce.getRequestText());
		}
		
		@Override
		/**
		  * PUT method, this function handles the PUT method
		  * @param ce: the CoAP Exchange 
		  */
		public void handlePUT(CoapExchange ce) {
			// string response message
			String responseMsg = "Here's the reponse to temp request::" + super.getName();
			// response code
			ce.respond(ResponseCode.VALID, responseMsg);
			// create logger
			_Logger.info("Handling PUT:" + responseMsg);
			// create logger
			_Logger.info(ce.getRequestCode().toString() + ": " + ce.getRequestText());
		}

		@Override
		 /**
		  * GET method, this function handles the GET method
		  * @param ce: the CoAP Exchange 
		  */
		public void handleGET(CoapExchange ce) {
			// string response message
			String responseMsg = "Here's the reponse to temp request::" + super.getName();
		    // send response code
			ce.respond(ResponseCode.VALID, responseMsg);
			// create logger
			_Logger.info("Handling GET:" + responseMsg);
			// create logger
			_Logger.info(ce.getRequestCode().toString() + ": " + ce.getRequestText());
			
		}

		@Override
		/**
		  * DELETE method, this function handles the DELETE method
		  * @param ce: the CoAP Exchange 
		  */
		public void handleDELETE(CoapExchange ce) {
			// string response message
			String responseMsg = "Here's the reponse to temp request::" + super.getName();
			 // send response code
			ce.respond(ResponseCode.VALID, responseMsg);
			// create logger
			_Logger.info("Handling DELETE:" + responseMsg);
			// create logger
			_Logger.info(ce.getRequestCode().toString() + ": " + ce.getRequestText());

		}

}
